#
# Copyright © 2018 Collabora Ltd.
#
# deqp_results is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 2.1 of the License, or (at your option) any later version.
#
# deqp_submit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with vkmark. If not, see <http://www.gnu.org/licenses/>.

import sys
import unittest
from os import path
import io

root = path.join(path.dirname(path.abspath(__file__)), "..")
sys.path.insert(0, root)

from deqp import Results

input = """{
  "Summary" : {
    "Fail" : 1,
    "Pass" : 2,
    "Unsupported" : 2
  },
  "Environment" : {
    "host-mesa" : "18.2.0-git111111"
  }
}
Test1 Fail
Test2 Pass
Test3 Pass
Test4 Unsupported
Test5 Unsupported
"""

input_unknown = """{
  "Summary" : {
    "CompatibilityWarning" : 1,
    "Fail" : 1,
    "Pass" : 2,
    "Unsupported" : 1
  },
  "Environment" : {
    "host-mesa" : "18.2.0-git111112"
  }
}
Test1 Fail
Test2 Pass
Test3 Pass
Test4 Unsupported
Test5 CompatibilityWarning
"""


input2 = """{
  "Summary" : {
    "Fail" : 0,
    "Pass" : 3,
    "Unsupported" : 1
  },
  "Environment" : {
    "host-mesa" : "18.2.0-git111112"
  }
}
Test1 Pass
Test2 Fail
Test3 Pass
Test4 Unsupported
Test6 Fail
"""

diff_1_unknown = """\x1b[9mTest5 \x1b[9mUnsupported\x1b[0m -> \x1b[9mCompatibilityWarning\x1b[0m
Change stats:
    Unsupported -> CompatibilityWarning:1
"""

update_1_with_2 = """{
  "Comments": {},
  "Environment": {
    "host-mesa": "18.2.0-git111112"
  },
  "Summary": {
    "Fail": 2,
    "Pass": 2,
    "Unsupported": 2
  },
  "Version": "deqp 1"
}
Test1 Pass
Test2 Fail
Test3 Pass
Test4 Unsupported
Test5 Unsupported
Test6 Fail
"""

update_1_with_unknown = """{
  "Comments": {},
  "Environment": {
    "host-mesa": "18.2.0-git111112"
  },
  "Summary": {
    "CompatibilityWarning": 1,
    "Fail": 1,
    "Pass": 2,
    "Unsupported": 1
  },
  "Version": "deqp 1"
}
Test1 Fail
Test2 Pass
Test3 Pass
Test4 Unsupported
Test5 CompatibilityWarning
"""


class ReadJson(unittest.TestCase):

    def test_read_simple(self):
        input_file = io.StringIO(input)
        data = Results.read(input_file)
        self.assertEqual(data._header[Results.SUMMARY]["Fail"], 1)
        self.assertEqual(data._header[Results.SUMMARY]["Pass"], 2)
        self.assertEqual(data._header[Results.SUMMARY]["Unsupported"], 2)
        self.assertEqual(data._header[Results.ENV]["host-mesa"],
                         '18.2.0-git111111')

        self.assertEqual(data._results["Test1"], "Fail")
        self.assertEqual(data._results["Test2"], "Pass")
        self.assertEqual(data._results["Test3"], "Pass")
        self.assertEqual(data._results["Test4"], "Unsupported")
        self.assertEqual(data._results["Test5"], "Unsupported")

    def test_update_1_with_2(self):
        input_file = io.StringIO(input)
        data1 = Results.read(input_file)

        input_file = io.StringIO(input2)
        data2 = Results.read(input_file)

        data1.update(data2)

        self.assertEqual(data1._header[Results.SUMMARY]["Fail"], 2)
        self.assertEqual(data1._header[Results.SUMMARY]["Pass"], 2)
        self.assertEqual(data1._header[Results.SUMMARY]["Unsupported"], 2)
        self.assertEqual(data1._header[Results.ENV]["host-mesa"],
                         '18.2.0-git111112')

        self.assertEqual(data1._results["Test1"], "Pass")
        self.assertEqual(data1._results["Test2"], "Fail")
        self.assertEqual(data1._results["Test3"], "Pass")
        self.assertEqual(data1._results["Test4"], "Unsupported")
        self.assertEqual(data1._results["Test5"], "Unsupported")
        self.assertEqual(data1._results["Test6"], "Fail")

    def test_update_1_with_2_output(self):
        input_file = io.StringIO(input)
        data1 = Results.read(input_file)

        input_file = io.StringIO(input2)
        data2 = Results.read(input_file)

        data1.update(data2)

        output_file = io.StringIO()
        data1.write(output_file)

        self.assertEqual(output_file.getvalue(), update_1_with_2)

    def test_update_1_with_unknown_output(self):
        input_file = io.StringIO(input)
        data1 = Results.read(input_file)

        input_file = io.StringIO(input_unknown)
        data2 = Results.read(input_file)

        data1.update(data2)

        output_file = io.StringIO()
        data1.write(output_file)

        self.assertEqual(output_file.getvalue(), update_1_with_unknown)

    def test_print_diff_with_unknown(self):
        input_file = io.StringIO(input)
        data1 = Results.read(input_file)

        input_file = io.StringIO(input_unknown)
        data2 = Results.read(input_file)

        self.assertEqual(data1.diff(data2), diff_1_unknown)


if __name__ == '__main__':
    unittest.main()
