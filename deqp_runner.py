#!/usr/bin/env python3

# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Copyright 2018 Collabora Ltd
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import bz2
import glob
import json
import logging
import os
import re
import shutil
import sys
import tempfile
import time
import utils
import xml.etree.ElementTree as et

RERUN_RATIO = 0.02  # Ratio to rerun failing test for hasty mode

class dEQPRunner:
    """Run the drawElements Quality Program test suite.
    """
    DEQP_MODULES = {
        'dEQP-EGL': 'egl',
        'dEQP-GLES2': 'gles2',
        'dEQP-GLES3': 'gles3',
        'dEQP-GLES31': 'gles31',
        'dEQP-VK': 'vk',
    }
    # We do not consider these results as failures.
    TEST_RESULT_FILTER = [
        'pass', 'notsupported', 'internalerror', 'qualitywarning',
        'compatibilitywarning', 'skipped'
    ]

    def __init__(self, cts_build_dir=None):
        self._surface = 'window'
        self._hasty = False
        self._hasty_batch_size = 100  # Batch size in hasty mode.
        self._shard_number = 0
        self._shard_count = 1
        self._filter = None
        self._width = 256  # Use smallest width for which all tests run/pass.
        self._height = 256  # Use smallest height for which all tests run/pass.
        self._timeout = 70  # Larger than twice the dEQP watchdog timeout at 30s.
        self._test_names = None
        self._test_names_file = None
        self._log_path = None  # Location for detailed test output logs (in /tmp/).
        self._env = None  # environment for test processes
        self._cts_build_dir = os.path.realpath(cts_build_dir)

    def _parse_test_results(self, result_filename,
                            test_results=None, failing_test=None,
                            detailed_results=None):
        """Handles result files with one or more test results.

        @param result_filename: log file to parse.
        @param test_results: Result parsed will be appended to it.
        @param failing_test: Tests considered failed will append to it.

        @return: dictionary of parsed test results.
        """
        xml = ''
        xml_start = False
        xml_complete = False
        xml_bad = False
        result = 'ParseTestResultFail'

        if test_results is None:
            test_results = {}

        if not os.path.isfile(result_filename):
            return test_results

        with open(result_filename, encoding='utf-8') as result_file:
            for line in result_file.readlines():
                # If the test terminates early, the XML will be incomplete
                # and should not be parsed.
                if line.startswith('#terminateTestCaseResult'):
                    result = line.strip().split()[1]
                    xml_bad = True
                # Will only see #endTestCaseResult if the test does not
                # terminate early.
                elif line.startswith('#endTestCaseResult'):
                    xml_complete = True
                elif xml_start:
                    xml += line
                elif line.startswith('#beginTestCaseResult'):
                    # If we see another begin before an end then something is
                    # wrong.
                    if xml_start:
                        xml_bad = True
                    else:
                        xml_start = True

                if xml_complete or xml_bad:
                    if xml_complete:
                        myparser = et.XMLParser(encoding='ISO-8859-1')
                        root = et.fromstring(xml, parser=myparser)
                        test_case = root.attrib['CasePath']
                        result = root.find('Result').get('StatusCode').strip()
                        xml_complete = False
                        if detailed_results is not None:
                            detailed_results[test_case] = result
                        if (result.lower() not in self.TEST_RESULT_FILTER and
                            failing_test != None):
                            failing_test.append(test_case)
                    test_results[result] = test_results.get(result, 0) + 1
                    xml_bad = False
                    xml_start = False
                    result = 'ParseTestResultFail'
                    xml = ''

        return test_results

    def _load_not_passing_cases(self, test_filter):
        """Load all test cases that are in non-'Pass' expectations."""
        not_passing_cases = []
        expectations_dir = os.path.join('expectations')
        subset_spec = '%s.*' % test_filter
        subset_paths = glob.glob(os.path.join(expectations_dir, subset_spec))
        for subset_file in subset_paths:
            # Filter against extra hasty failures only in hasty mode.
            if (not '.Pass.bz2' in subset_file and
               (self._hasty or '.hasty.' not in subset_file)):
                not_passing_cases.extend(
                    bz2.BZ2File(subset_file).read().splitlines())
        not_passing_cases.sort()
        return not_passing_cases

    def _translate_name_to_api(self, name):
        """Translate test_names or test_filter to api."""
        test_prefix = name.split('.')[0]
        if test_prefix in self.DEQP_MODULES:
            api = self.DEQP_MODULES[test_prefix]
        else:
            raise error.TestFail('Failed: Invalid test name: %s' % name)
        return api

    def _get_executable(self, api):
        """Return the executable path of the api."""
        rel_path = 'modules/%s/deqp-%s' % (api, api)
        return os.path.join(self._cts_build_dir, rel_path)

    def _bootstrap_new_test_cases(self, test_filter):
        """Ask dEQP for all test cases and removes non-Pass'ing ones.

        This function will query dEQP for test cases and remove all cases that
        are not in 'Pass'ing expectations from the list. This can be used
        incrementally updating failing/hangin tests over several runs.

        @param test_filter: string like 'dEQP-GLES2.info', 'dEQP-GLES3.stress'.

        @return: List of dEQP tests to run.
        """
        test_cases = []
        api = self._translate_name_to_api(test_filter)
        executable = self._get_executable(api)

        # Must be in the executable directory when running for it to find it's
        # test data files!
        os.chdir(os.path.dirname(executable))

        not_passing_cases = self._load_not_passing_cases(test_filter)
        # We did not find passing cases in expectations. Assume everything else
        # that is there should not be run this time.
        expectations_dir = os.path.join('expectations')
        subset_spec = '%s.*' % test_filter
        subset_paths = glob.glob(os.path.join(expectations_dir, subset_spec))
        for subset_file in subset_paths:
            # Filter against hasty failures only in hasty mode.
            if self._hasty or '.hasty.' not in subset_file:
                not_passing_cases.extend(
                    bz2.BZ2File(subset_file).read().splitlines())

        # Now ask dEQP executable nicely for whole list of tests. Needs to be
        # run in executable directory. Output file is plain text file named
        # e.g. 'dEQP-GLES2-cases.txt'.
        command = ('%s '
                   '--deqp-runmode=txt-caselist '
                   '--deqp-surface-type=%s '
                   '--deqp-gl-config-name=rgba8888d24s8ms0 ' % (executable,
                                                                self._surface))
        logging.info('Running command %s', command)
        utils.run(command,
                  env=self._env,
                  timeout=60,
                  stderr_is_expected=False,
                  ignore_status=False,
                  stdin=None)

        # Now read this caselist file.
        caselist_name = '%s-cases.txt' % test_filter.split('.')[0]
        caselist_file = os.path.join(os.path.dirname(executable), caselist_name)
        if not os.path.isfile(caselist_file):
            raise error.TestFail('Failed: No caselist file at %s!' %
                                 caselist_file)

        # And remove non-Pass'ing expectations from caselist.
        caselist = open(caselist_file).read().splitlines()
        # Contains lines like "TEST: dEQP-GLES2.capability"
        test_cases = []
        match = 'TEST: %s' % test_filter
        logging.info('Bootstrapping test cases matching "%s".', match)
        for case in caselist:
            if case.startswith(match):
                case = case.split('TEST: ')[1]
                test_cases.append(case)

        test_cases = list(set(test_cases) - set(not_passing_cases))
        if not test_cases:
            raise RuntimeError('Failed: Unable to bootstrap %s!' % test_filter)

        test_cases.sort()
        return test_cases

    def _get_test_cases_from_names_file(self):
        if os.path.exists(self._test_names_file):
            file_path = self._test_names_file
            test_cases = [line.rstrip('\n') for line in open(file_path)]
            return [test for test in test_cases if test and not test.isspace()]
        return []

    def _get_test_cases(self, test_filter, subset):
        """Gets the test cases for 'Pass', 'Fail' etc. expectations.

        This function supports bootstrapping of new GPU families and dEQP
        binaries. In particular if there are not 'Pass' expectations found for
        this GPU family it will query the dEQP executable for a list of all
        available tests. It will then remove known non-'Pass'ing tests from
        this list to avoid getting into hangs/crashes etc.

        @param test_filter: string like 'dEQP-GLES2.info', 'dEQP-GLES3.stress'.
        @param subset: string from 'Pass', 'Fail', 'Timeout' etc.

        @return: List of dEQP tests to run.
        """
        expectations_dir = os.path.join('expectations')
        subset_name = '%s.%s.bz2' % (test_filter, subset)
        subset_path = os.path.join(expectations_dir, subset_name)
        if not os.path.isfile(subset_path):
            if subset == 'NotPass':
                # TODO(ihf): Running hasty and NotPass together is an invitation
                # for trouble (stability). Decide if it should be disallowed.
                return self._load_not_passing_cases(test_filter)
            if subset != 'Pass':
                raise error.TestFail('Failed: No subset file found for %s!' %
                                     subset_path)
            # Ask dEQP for all cases and remove the failing ones.
            return self._bootstrap_new_test_cases(test_filter)

        test_cases = bz2.BZ2File(subset_path).read().splitlines()
        if not test_cases:
            raise error.TestFail(
                'Failed: No test cases found in subset file %s!' % subset_path)
        return test_cases

    def _run_tests_individually(self, test_cases, failing_test=None):
        """Runs tests as isolated from each other, but slowly.

        This function runs each test case separately as a command.
        This means a new context for each test etc. Failures will be more
        isolated, but runtime quite high due to overhead.

        @param test_cases: List of dEQP test case strings.
        @param failing_test: Tests considered failed will be appended to it.

        @return: dictionary of test results.
        """
        test_results = {}
        width = self._width
        height = self._height

        i = 0
        for test_case in test_cases:
            i += 1
            logging.info('[%d/%d] TestCase: %s', i, len(test_cases), test_case)
            result_prefix = os.path.join(self._log_path, test_case)
            log_file = '%s.log' % result_prefix
            debug_file = '%s.debug' % result_prefix
            api = self._translate_name_to_api(test_case)
            executable = self._get_executable(api)
            command = ('%s '
                       '--deqp-case=%s '
                       '--deqp-surface-type=%s '
                       '--deqp-gl-config-name=rgba8888d24s8ms0 '
                       '--deqp-log-images=disable '
                       '--deqp-watchdog=enable '
                       '--deqp-surface-width=%d '
                       '--deqp-surface-height=%d '
                       '--deqp-visibility=%s '
                       '--deqp-log-filename=%s' % (
                           executable,
                           test_case,
                           self._surface,
                           width,
                           height,
                           'hidden' if self._invisible else 'windowed',
                           log_file)
                       )
            logging.debug('Running single: %s', command)

            # Must be in the executable directory when running for it to find it's
            # test data files!
            os.chdir(os.path.dirname(executable))

            # Must initialize because some errors don't repopulate
            # run_result, leaving old results.
            run_result = {}
            start_time = time.time()
            try:
                run_result = utils.run(command,
                                       env=self._env,
                                       timeout=self._timeout,
                                       stderr_is_expected=False,
                                       ignore_status=True)
                result_counts = self._parse_test_results(
                    log_file,
                    failing_test=failing_test)
                if result_counts:
                    result = list(result_counts.keys())[0]
                else:
                    result = 'Unknown'
            except utils.TimeoutError:
                result = 'Timeout'
            except Exception:
                result = 'UnexpectedError'
            end_time = time.time()

            if self._debug:
                # Collect debug info and save to json file.
                output_msgs = {
                    'start_time': start_time,
                    'end_time': end_time,
                    'stdout': [],
                    'stderr': [],
                }
                if run_result:
                    output_msgs['stdout'] = run_result.stdout.splitlines()
                    output_msgs['stderr'] = run_result.stderr.splitlines()
                with open(debug_file, 'w') as fd:
                    json.dump(
                        output_msgs,
                        fd,
                        indent=4,
                        separators=(',', ' : '),
                        sort_keys=True)

            logging.info('Result: %s', result)
            test_results[result] = test_results.get(result, 0) + 1

        return test_results

    def _run_tests_hasty(self, test_cases, failing_test=None):
        """Runs tests as quickly as possible.

        This function runs all the test cases, but does not isolate tests and
        may take shortcuts/not run all tests to provide maximum coverage at
        minumum runtime.

        @param test_cases: List of dEQP test case strings.
        @param failing_test: Test considered failed will append to it.

        @return: dictionary of test results.
        """
        # TODO(ihf): It saves half the test time to use 32*32 but a few tests
        # fail as they need surfaces larger than 200*200.
        width = self._width
        height = self._height
        results = {}

        # All tests combined less than 1h in hasty.
        batch_timeout = min(3600, self._timeout * self._hasty_batch_size)
        num_test_cases = len(test_cases)

        # We are dividing the number of tests into several shards but run them
        # in smaller batches. We start and end at multiples of batch_size
        # boundaries.
        shard_start = self._hasty_batch_size * (
            (self._shard_number * (num_test_cases / self._shard_count)) /
            self._hasty_batch_size)
        shard_end = self._hasty_batch_size * ((
            (self._shard_number + 1) * (num_test_cases / self._shard_count)) /
                                              self._hasty_batch_size)
        # The last shard will be slightly larger than the others. Extend it to
        # cover all test cases avoiding rounding problems with the integer
        # arithmetics done to compute shard_start and shard_end.
        if self._shard_number + 1 == self._shard_count:
            shard_end = num_test_cases

        shard_start = int(shard_start)
        shard_end = int(shard_end)

        for batch in range(shard_start, shard_end, self._hasty_batch_size):
            batch_to = min(batch + self._hasty_batch_size, shard_end)
            batch_cases = '\n'.join(test_cases[batch:batch_to])
            # This assumes all tests in the batch are kicked off via the same
            # executable.
            api = self._translate_name_to_api(test_cases[batch])
            executable = self._get_executable(api)
            log_file = os.path.join(self._log_path,
                                    '%s_hasty_%d.log' % (self._filter, batch))
            command = ('%s '
                       '--deqp-stdin-caselist '
                       '--deqp-surface-type=%s '
                       '--deqp-gl-config-name=rgba8888d24s8ms0 '
                       '--deqp-log-images=disable '
                       '--deqp-visibility=hidden '
                       '--deqp-watchdog=enable '
                       '--deqp-surface-width=%d '
                       '--deqp-surface-height=%d '
                       '--deqp-log-filename=%s' % (
                           executable,
                           self._surface,
                           width,
                           height,
                           log_file)
                       )

            logging.info('Running tests %d...%d out of %d:\n%s\n%s',
                         batch + 1, batch_to, num_test_cases, command,
                         batch_cases)

            # Must be in the executable directory when running for it to
            # find it's test data files!
            os.chdir(os.path.dirname(executable))

            try:
                utils.run(command,
                          env=self._env,
                          timeout=batch_timeout,
                          stderr_is_expected=False,
                          ignore_status=False,
                          stdin=batch_cases)
            except Exception as e:
                logging.warning('Test suite failed with: %s', e)
                logging.warning('Results may be incomplete')

            # We are trying to handle all errors by parsing the log file.
            detailed_results = {}
            results = self._parse_test_results(log_file, results,
                                               failing_test,
                                               detailed_results=detailed_results)

            for i,(test_case, result) in enumerate(detailed_results.items()):
                logging.info('[%d/%d] TestCase: %s', batch + i + 1, len(test_cases), test_case)
                logging.info('Result: %s', result)

        return results

    def _run_once(self, test_cases):
        """Run dEQP test_cases in individual/hasty mode.
        @param test_cases: test cases to run.
        """
        failing_test = []
        if self._hasty:
            logging.info('Running in hasty mode.')
            test_results = self._run_tests_hasty(test_cases, failing_test)
        else:
            logging.info('Running each test individually.')
            test_results = self._run_tests_individually(test_cases,
                                                        failing_test)
        return test_results, failing_test

    def run_once(self, opts=None):
        options = dict(filter='',
                       test_names='',  # e.g., dEQP-GLES3.info.version,
                                       # dEQP-GLES2.functional,
                                       # dEQP-GLES3.accuracy.texture, etc.
                       test_names_file='',
                       timeout=self._timeout,
                       subset_to_run='Pass',  # Pass, Fail, Timeout, NotPass...
                       hasty='False',
                       hasty_batch_size='100',
                       shard_number='0',
                       shard_count='1',
                       debug='False',)
        if opts is None:
            opts = {}
        options.update(opts)
        logging.info('Test Options: %s', options)

        self._hasty = (options['hasty'] == 'True')
        self._timeout = int(options['timeout'])
        self._test_names_file = options['test_names_file']
        self._test_names = options['test_names']
        self._shard_number = int(options['shard_number'])
        self._shard_count = int(options['shard_count'])
        self._debug = (options['debug'] == 'True')
        self._invisible = (options['invisible'] == 'True')
        self._hasty_batch_size = int(options['hasty_batch_size'])
        if not (self._test_names_file or self._test_names):
            self._filter = options['filter']
            if not self._filter:
                raise error.TestFail('Failed: No dEQP test filter specified')

        # Create a place to put detailed test output logs.
        filter_name = self._filter or os.path.basename(self._test_names_file)
        logging.info('dEQP test filter = %s', filter_name)
        self._log_path = os.path.join(tempfile.gettempdir(), '%s-logs' %
                                                             filter_name)
        shutil.rmtree(self._log_path, ignore_errors=True)
        os.mkdir(self._log_path)

        # Load either tests specified by test_names_file, test_names or filter.
        test_cases = []
        if self._test_names_file:
            test_cases = self._get_test_cases_from_names_file()
        elif self._test_names:
            test_cases = []
            for name in self._test_names.split(','):
                test_cases.extend(self._get_test_cases(name, 'Pass'))
        elif self._filter:
            test_cases = self._get_test_cases(self._filter,
                                              options['subset_to_run'])

        test_results, failing_test = self._run_once(test_cases)
        # Rerun the test if we are in hasty mode.
        if self._hasty and len(failing_test) > 0:
            if len(failing_test) < sum(test_results.values()) * RERUN_RATIO:
                logging.info("Because we are in hasty mode, we will rerun the "
                             "failing tests one at a time")
                rerun_results, failing_test = self._run_once(failing_test)
                # Update failing test result from the test_results
                for result in test_results:
                    if result.lower() not in self.TEST_RESULT_FILTER:
                        test_results[result] = 0
                for result in rerun_results:
                    test_results[result] = (test_results.get(result, 0) +
                                            rerun_results[result])
            else:
                logging.info("There are too many failing tests. It would "
                             "take too long to rerun them. Giving up.")

        logging.info('Test results:')
        logging.info(test_results)
        logging.debug('Test Failed: %s', failing_test)

        test_count = 0
        test_failures = 0
        test_passes = 0
        test_skipped = 0
        for result in test_results:
            test_count += test_results[result]
            if result.lower() in ['pass']:
                test_passes += test_results[result]
            if result.lower() not in self.TEST_RESULT_FILTER:
                test_failures += test_results[result]
            if result.lower() in ['skipped']:
                test_skipped += test_results[result]
        # The text "Completed all tests." is used by the process_log.py script
        # and should always appear at the end of a completed test run.
        logging.info(
            'Completed all tests. Saw %d tests, %d passes and %d failures.',
            test_count, test_passes, test_failures)

        if self._filter and test_count == 0 and options[
                'subset_to_run'] != 'NotPass':
            logging.warning('No test cases found for filter: %s!', self._filter)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)

    parser = argparse.ArgumentParser()
    parser.add_argument('filter', nargs='?')
    parser.add_argument('--hasty', action='store_true',
                        help='Run in hasty (batch) mode (also implies --invisible)')
    parser.add_argument('--hasty-batch-size', default='100',
                        help='Batch size in  hasty (batch) mode')
    parser.add_argument('--cts-build-dir', default='.', help='The build dir path of the CTS')
    parser.add_argument('--test-names-file', help='File to read the test names from')
    parser.add_argument('--timeout', default='70', help='Timeout for each test')
    parser.add_argument('--invisible', action='store_true',
                        help="Don't show a window for each test")

    args = parser.parse_args(sys.argv[1:])

    runner = dEQPRunner(cts_build_dir=args.cts_build_dir)
    opts = {'filter': args.filter, 'hasty': str(args.hasty),
            'test_names_file': args.test_names_file,
            'timeout': args.timeout, 'invisible': str(args.invisible),
            'hasty_batch_size': str(args.hasty_batch_size)}
    runner.run_once(opts)
