#
# Copyright © 2018 Collabora Ltd.
#
# deqp_results is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 2.1 of the License, or (at your option) any later version.
#
# deqp_submit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with vkmark. If not, see <http://www.gnu.org/licenses/>.

import json
import re
import sys


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    STRIKETHROUGH = '\033[9m'
    UNKNOWN = '\033[4m'


class Results:
    "Results is a class for working with results files"

    SUMMARY = "Summary"
    ENV = "Environment"
    COMMENTS = "Comments"
    VERSION = "Version"

    StateColors = {
        "Fail": bcolors.FAIL,
        "InternalError": bcolors.WARNING,
        "Incomplete": bcolors.WARNING,
        "NotSupported": bcolors.STRIKETHROUGH,
        "Pass": bcolors.OKGREEN,
        "QualityWarning": bcolors.OKBLUE,
        "Unknown": bcolors.UNKNOWN
        }

    def __init__(self, results=None, header=None):
        if results is None:
            self._results = {}
        else:
            self._results = results

        if header is None:
            self._header = {}
            self._header[self.VERSION] = "deqp 1"
            self._header = {}
            self._header[self.SUMMARY] = {}
            self._header[self.ENV] = {}
            self._header[self.COMMENTS] = {}
        else:
            self._header = header
            if self.VERSION not in self._header:
                self._header[self.VERSION] = "deqp 1"
            if self.COMMENTS not in self._header:
                self._header[self.COMMENTS] = {}
            if self.ENV not in self._header:
                self._header[self.ENV] = {}
            if self.SUMMARY not in self._header:
                self._header[self.SUMMARY] = {}

    def update_one_result(self, test, outcome):
        self._results[test] = outcome

    def _add_header_entry(self, category, key, value):
        self._header[category][key] = value

    def add_comment(self, key, comment):
        self._add_header_entry(self.COMMENTS, key, comment)

    def add_env(self, key, env):
        self._add_header_entry(self.ENV, key, env)

    def add_summary(self, key, value):
        self._add_header_entry(self.SUMMARY, key, value)

    def update_result_counts(self):
        result_counts = {}
        for (t, r) in self._results.items():
            c = result_counts.setdefault(r, 0)
            result_counts[r] = c + 1

        self._header[self.SUMMARY] = result_counts

    def update(self, results):
        self._results = {**self._results, **results._results}
        self.update_result_counts()

        if len(results._header[self.ENV]) > 0:
            self._header[self.ENV].update(results._header[self.ENV])

    def get_key_sets(self, results):
        old_keys = set(self._results.keys())
        new_keys = set(results._results.keys())
        shared_keys = old_keys.intersection(new_keys)
        return (shared_keys,
                new_keys.difference(shared_keys),
                old_keys.difference(shared_keys))

    def update_with_compare(self, results):
        change_counts = {}
        (shared_keys, new_keys, dropped_keys) = self.get_key_sets(results)

        for k in shared_keys:
            oldval = self._results[k]
            newval = results._results[k]
            if oldval != newval:
                change = "{} -> {}".format(oldval, newval)

                c = change_counts.setdefault(change, 0)
                change_counts[change] = c + 1

        if len(new_keys) > 0:
            change_counts["Tests new"] = len(new_keys)
        if len(dropped_keys) > 0:
            change_counts["Tests dropped"] = len(dropped_keys)

        self.update(results)
        self._header[self.COMMENTS]["Difference"] = change_counts
        return change_counts

    def diff(self, results):
        out_string = ""
        change_counts = {}
        (shared_keys, new_keys, dropped_keys) = self.get_key_sets(results)

        for k in sorted(shared_keys):
            oldval = self._results[k]
            newval = results._results[k]
            col = bcolors.ENDC
            if oldval != newval:
                if newval == "Fail":
                    col = bcolors.FAIL
                elif newval == "Pass":
                    col = bcolors.OKGREEN
                elif newval == "QualityWarning":
                    if "oldval" == "Pass":
                        col = bcolors.WARNING
                    else:
                        col = bcolors.OKBLUE
                elif newval == "NotSupported":
                    col = bcolors.STRIKETHROUGH
                elif newval == "Incomplete" or newval == "InternalError":
                    if oldval == "Pass" or oldval == "QualityWarning":
                        col = bcolors.FAIL
                    else:
                        col = bcolors.UNDERLINE
                else:
                    col = bcolors.UNKNOWN

                change = "{} -> {}".format(oldval, newval)
                c = change_counts.setdefault(change, 0)
                change_counts[change] = c + 1

                # Only print colors when on a TTY
                if sys.stdout.isatty():
                    oldval = self.StateColors.get(oldval, bcolors.UNKNOWN) + oldval + bcolors.ENDC
                    newval = self.StateColors.get(newval, bcolors.UNKNOWN) + newval + bcolors.ENDC
                    out_string = out_string + "{}{} {} -> {}\n".format(col, k, oldval, newval)
                else:
                    out_string = out_string + "{} {} -> {}\n".format(k, oldval, newval)

        if len(new_keys) > 0:
            change_counts["Tests new"] = len(new_keys)
        if len(dropped_keys) > 0:
            change_counts["Tests dropped"] = len(dropped_keys)

        out_string = out_string + "Change stats:\n"
        for key, value in sorted(change_counts.items()):
            out_string = out_string + "    {}:{}\n".format(key, value)
        return out_string

    @staticmethod
    def atoi(t):
        return int(t) if t.isdigit() else t

    @staticmethod
    def natural_key_for_tuple0(r):
            text = r[0]
            return [Results.atoi(c) for c in re.split('(\d+)', text)]

    def write(self, f):
        self.update_result_counts()
        f.write(json.dumps(self._header, sort_keys=True, indent=2))
        f.write("\n")
        for (t, r) in sorted(self._results.items(),
                             key=self.natural_key_for_tuple0):
            f.write("{} {}\n".format(t, r))

    @staticmethod
    def read(f):
        head = f.read(10000)
        (header, offset) = json.JSONDecoder().raw_decode(head)
        f.seek(offset)
        results = dict([x.split() for x in f.readlines() if len(x.split()) == 2 ])
        return Results(results, header)
