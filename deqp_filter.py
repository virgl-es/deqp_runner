#!/usr/bin/env python3

# Copyright © 2018 Collabora Ltd.
#
# deqp_filter is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 2.1 of the License, or (at your option) any later version.
#
# deqp_filter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with vkmark. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import argparse
import re

def atoi(t):
    return int(t) if t.isdigit() else t
# Natural sorting implementation
def natural_key_for_tuple0(r):
    text = r[0]
    return [atoi(c) for c in re.split('(\d+)', text)]

def read_results_file(f):
    results = {}

    for line in f:
        if line.startswith('#'):
            continue
        s = line.split()
        if len(s) == 2:
            results[s[0]] = s[1]

    return results

def read_test_names_file(f):
    test_names = set()

    for line in f:
        test_names.add(line.strip())

    return test_names

def write_results_file_stats(f, results):
    result_counts = {}

    for (t, r) in results.items():
        c = result_counts.setdefault(r, 0)
        result_counts[r] = c + 1

    for (r, c) in sorted(result_counts.items(), key=lambda r: r[0]):
        f.write("# %s %s\n" % (r, c))

def write_results_file_results(f, results):
    for (t, r) in sorted(results.items(), key=natural_key_for_tuple0):
        f.write("%s %s\n" % (t, r))

def write_results_file_missing(f, missing):
    for t in sorted(missing, key=natural_key_for_tuple0):
        f.write("# Missing %s\n" % t)

def write_results_file(f, results, missing):
    write_results_file_stats(f, results)
    write_results_file_missing(f, missing)
    write_results_file_results(f, results)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda x: parser.print_help())
    parser.add_argument('results_file', nargs='?')
    parser.add_argument('--test-names-file', required=True)

    args = parser.parse_args(sys.argv[1:])

    test_names = set()

    with open(os.path.abspath(args.test_names_file), "r") as f:
        test_names = read_test_names_file(f)

    if args.results_file:
        results_file = open(os.path.abspath(args.results_file), "r")
    else:
        results_file = sys.stdin

    results = read_results_file(results_file)
    results_key_set = set(results.keys())

    filtered_results = { k: results[k] for k in results_key_set & test_names }
    missing_result_set = test_names - results_key_set

    write_results_file(sys.stdout, filtered_results, missing_result_set)
