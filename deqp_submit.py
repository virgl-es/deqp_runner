#!/usr/bin/env python3

# Copyright © 2018 Collabora Ltd.
#
# deqp_submit is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 2.1 of the License, or (at your option) any later version.
#
# deqp_submit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with vkmark. If not, see <http://www.gnu.org/licenses/>.

import argparse
import contextlib
import os
import sh
import sys
import xml.etree.ElementTree as et

sys.path.insert(0, os.path.realpath(__file__))

from deqp import Results

XDG_CACHE_HOME = os.environ.get('XDG_CACHE_HOME') or \
                 os.path.expandvars(os.path.join('$HOME', '.cache'))

git_repo = os.path.abspath(os.path.join(XDG_CACHE_HOME,
                                        'deqp_submit/deqp_results'))
git_url = 'git@gitlab.collabora.com:virgl-es/deqp_results'


@contextlib.contextmanager
def log(message):
    print(message, file=sys.stderr, end='')
    print("...", end='', file=sys.stderr, flush=True)
    yield
    print(" OK", file=sys.stderr, )


class Git:
    def __init__(self, repo, url, email=None):
        self.repo = repo
        self.url = url
        self.git = sh.git.bake("--no-pager", _cwd=self.repo)

        if os.path.isdir(self.repo):
            with log("Updating existing results repo in '%s'" % self.repo):
                sh.git.pull("--rebase=true", _cwd=self.repo)
        else:
            repo_parent = os.path.abspath(os.path.join(self.repo, os.pardir))
            os.makedirs(repo_parent, exist_ok=True)
            with log("Cloning results repo in '%s'" % self.repo):
                sh.git.clone(url, _cwd=repo_parent)

        if email is not None:
            with log("set email to {}".format(email)):
                sh.git.config('user.email', email, _cwd=self.repo)

    def add(self, path):
        self.git.add(path)

    def commit(self, path, message):
        self.git.add(path)
        self.git.commit("-m", message, "--", path)

    def upload(self):
        self.git.push("origin", "master")

    def diff_iter(self):
        return self.git.diff("HEAD", _iter=True)

    def reset_hard(self):
        self.git.reset("--hard")


def get_results_dir(repo):
    if os.path.isdir(repo):
        email = sh.git.config('user.email', _cwd=repo)
    else:
        email = sh.git.config('user.email')
    s = email.split('@')
    if len(s) < 2:
        raise RuntimeError("Invalid or missing git user.email")

    return os.path.join(repo, s[0])


def open_file(path, flags):
    fullpath = os.path.abspath(path)
    parent = os.path.abspath(os.path.join(fullpath, os.pardir))
    os.makedirs(parent, exist_ok=True)
    return open(fullpath, flags)


def read_results_file(f):
    results = {}

    for line in f:
        if line.startswith('#'):
            continue
        s = line.split()
        if len(s) == 2:
            results[s[0]] = s[1]

    return Results(results)


def parse_input_file_xml(f):
    xml = ''
    xml_start = False
    xml_complete = False
    xml_bad = False
    results = {}

    for line in f:
        # If the test terminates early, the XML will be incomplete
        # and should not be parsed.
        if line.startswith('#terminateTestCaseResult'):
            result = line.strip().split()[1]
            xml_bad = True
        # Will only see #endTestCaseResult if the test does not
        # terminate early.
        elif line.startswith('#endTestCaseResult'):
            xml_complete = True
        elif xml_start:
            xml += line
        elif line.startswith('#beginTestCaseResult'):
            # If we see another begin before an end then something is
            # wrong.
            if xml_start:
                xml_bad = True
            else:
                xml_start = True

        if xml_complete or xml_bad:
            if xml_complete:
                myparser = et.XMLParser(encoding='ISO-8859-1')
                root = et.fromstring(xml, parser=myparser)
                test_case = root.attrib['CasePath']
                result = root.find('Result').get('StatusCode').strip()
                xml_complete = False
                results[test_case] = result
            xml_bad = False
            xml_start = False
            xml = ''

    return Results(results)


def parse_input_file_deqp_runner(f):
    last_test_case = ''
    last_result = ''
    results = {}

    for line in f:
        if '] TestCase:' in line:
            s = line.split()
            if len(s) == 3:
                last_test_case = s[2]
        elif ':Result:' in line:
            s = line.split()
            if len(s) == 2:
                last_result = s[1]
                results[last_test_case] = last_result
        elif ':Results may be incomplete' in line:
            print('Warning: Results in input file may be incomplete',
                  file=sys.stderr)

    return Results(results)


def parse_input_file_volt_deqp(f):
    results = {}

    for line in f:
        if line[0] != '#':
            s = line.split()
            if len(s) == 2:
                results[s[0]] = s[1]
            else:
                print('Warning: Results in input file may be incomplete',
                      file=sys.stderr)
    return Results(results)


def parse_input_file(f):
    # Search for the xml results file markers in the first 10KB (max)
    head = f.read(10000)
    f.seek(0)
    if head[0] == '{':
        return Results.read(f)
    elif '#beginTestCaseResult' in head:
        return parse_input_file_xml(f)
    elif head.startswith('# Fail') or head.startswith('# Com') :
        return parse_input_file_volt_deqp(f)
    else:
        return parse_input_file_deqp_runner(f)


def get_old_results(results_path):
    results = None
    if os.path.isfile(results_path):
        with log("Reading existing results from '%s'" % results_path):
            with open_file(results_path, "r") as fout:
                results = parse_input_file(fout)
    return results


def save_results(results_path, results):
    with log("Writing results to '%s'" % results_path):
        with open_file(results_path, "w") as fout:
            results.write(fout)


def read_results(git, args):
    results_dir_full = get_results_dir(git.repo)
    results_path = os.path.join(results_dir_full, args.results_file)

    results = get_old_results(results_path)
    new_results = None

    for f in args.file:
        with log("Reading new results from '%s'" % f):
            with open(f, "r") as fin:
                new_results = parse_input_file(fin)
    return (results_path, results, new_results)


def print_diff(git, args):
    (results_path, results, new_results) = read_results(git, args)
    if (new_results is not None) and (results is not None):
        print(results.diff(new_results))
    else:
        log("No results to compare")


def update_results_file(git, args):
    (results_path, results, new_results) = read_results(git, args)
    diff_info = None

    if new_results is not None:
        if results is not None:
            diff_info = results.update_with_compare(new_results)
        else:
            results = new_results
        save_results(results_path, results)

    return (results_path, diff_info)


def cmd_upload(args):
    git = Git(git_repo, git_url, args.email)

    (results_path, diff_info) = update_results_file(git, args)

    try:
        with log("Commiting results to git"):
            git.commit(results_path, args.message)

        if not args.commit_only:
            with log("Uploading results to gitlab"):
                git.upload()

        if diff_info is not None:
            print("Change stats:")
            for key, value in sorted(diff_info.items()):
                print("    {}:{}".format(key, value))
    except:
        git.reset_hard()
        raise


def cmd_diff(args):
    git = Git(git_repo, git_url)
    print_diff(git, args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda x: parser.print_help())
    subparsers = parser.add_subparsers()

    parser_diff = subparsers.add_parser(
        "diff", description="Show diff of new results vs upstream")
    parser_diff.add_argument('file', nargs='+')
    parser_diff.add_argument('--results-file', required=True,
                             help='The name of the results file')
    parser_diff.set_defaults(func=cmd_diff)

    parser_upload = subparsers.add_parser(
        "upload", description="Upload new results")
    parser_upload.add_argument('file', nargs='+')
    parser_upload.add_argument('--results-file', required=True,
                               help='The name of the results file')
    parser_upload.add_argument('--message', required=True)
    parser_upload.add_argument('--commit-only', action='store_true')
    parser_upload.add_argument('--email')

    parser_upload.set_defaults(func=cmd_upload)

    args = parser.parse_args(sys.argv[1:])

    try:
        args.func(args)
    except Exception as e:
        print("Error: %s" % e)
